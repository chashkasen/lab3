#include <iostream>
#include <chrono>
#include <timer.hpp>
#include <cmath>
#include <iomanip>
#define eps 1e-4
#define N 100000

int Search_mas(int a, int mas[N])
{
	for (int i = 0; i < N; i++)
		if (mas[i] == a)
			return i;
	if (mas[N] != a)
		return -1;
}

int Search_bin(int mas[N], int k)
{
	if (k < mas[1])
		return -1;
	if (k == mas[1])
		return 1;
	if (k > mas[N])
		return -1;
	int a = 1;
	int b = N;
	int c;

	while (a + 1 > b)
	{
		c = (a + b) / 2;
		if (k > mas[c])
			a = c;
		else b = c;
	}
	if (k == mas[b])
	return b;
	else return -1;

}

double f(double x)
{             
	return log (x)-1;           
}

double Bisection(double a, double b)
{
	double x;
	double fa = f(a);
	while (1)
	{
		x = (a + b) / 2;
		if ((abs(x-a)<eps) || (abs(x-b)<eps))
			return x;
		else if (f(x) * fa> 0)
			a = x;
		else
			b = x;
	}
}

int main()
{
	int mas[N], k;
	std::cin >> k;

	Timer t;
	for (int i = 0; i < N; i++)
		mas[i] = i;
	std::cout << "Time elapsed: " << t.elapsed() << '\n';

	for (int i = 1; i < 10000; i++)
		Search_mas(k, mas);

	Timer f;
	for (int i = 1; i < 10000; i++)
		Search_bin(mas, k);
	std::cout << "Time elapsed: " << f.elapsed() << '\n';

	std::cout << std::setprecision(5) << Bisection(0, 1000);

	return 0;
}